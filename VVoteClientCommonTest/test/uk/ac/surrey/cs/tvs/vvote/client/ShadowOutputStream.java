package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.OutputStream;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;

/**
 * Shadow Implementation for <code>OutputStream</code>
 */
@Implements(OutputStream.class)
public class ShadowOutputStream {

	/**
	 * Flag for whether write has been called
	 */
	private boolean writeCalled = false;

	/**
	 * Flag for whether write with a provided offset and length has been called
	 */
	private boolean writeWithOffsetAndLengthCalled = false;

	/**
	 * Constructor
	 */
	public ShadowOutputStream() {
		super();
	}

	/**
	 * Shadow Implementation for OutputStream.write(byte[])
	 * 
	 * @param buffer
	 */
	@Implementation
	public void write(byte[] buffer) {
		this.writeCalled = true;
	}

	/**
	 * Shadow Implementation for OutputStream.write(byte[],int,int)
	 * 
	 * @param b
	 * @param off
	 * @param len
	 */
	@Implementation
	public void write(byte b[], int off, int len) {
		this.writeWithOffsetAndLengthCalled = true;
	}

	/**
	 * Getter for whether write has been caled
	 * 
	 * @return writeCalled
	 */
	public boolean hasWriteBeenCalled() {
		return this.writeCalled;
	}

	/**
	 * Getter for whether writeWithOffsetAndLengthCalled has been caled
	 * 
	 * @return writeWithOffsetAndLengthCalled
	 */
	public boolean hasWriteWithOffsetAndLengthCalledBeenCalled() {
		return this.writeWithOffsetAndLengthCalled;
	}
}
