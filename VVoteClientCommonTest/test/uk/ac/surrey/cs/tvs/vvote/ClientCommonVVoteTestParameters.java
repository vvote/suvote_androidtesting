package uk.ac.surrey.cs.tvs.vvote;

import android.graphics.Bitmap;

/**
 * Holds a number of test parameters used throughout the vvote package
 */
public class ClientCommonVVoteTestParameters {

	/**
	 * Dummy exception message used for testing.
	 */
	public static final String EXCEPTION_MESSAGE = "Exception Message";

	/**
	 * Name of the field storing the EpsonThermalPrinter
	 */
	public static final String EPSON_THERMAL_PRINTER_FIELD = "etp";

	/**
	 * Key for the parameter map for image content
	 */
	public static final String IMAGE_CONTENT_KEY = "imageContent";

	/**
	 * Value for the parameter map for image content
	 */
	public static final String IMAGE_CONTENT_VALUE = "test";

	/**
	 * Name of the service config used as the config file
	 */
	public static final String SERVICE_CONF = "serviceConfig.conf";

	/**
	 * Size of the buffer specified in "serviceConfig.conf"
	 */
	public static final int SERVICE_CONF_NEW_BUFFER_SIZE = 5;

	/**
	 * Image config
	 */
	public static final Bitmap.Config IMAGE_CONFIG = Bitmap.Config.ALPHA_8;

	/**
	 * Image width
	 */
	public static final int IMAGE_WIDTH = 32;

	/**
	 * Image height
	 */
	public static final int IMAGE_HEIGHT = 32;

	/**
	 * Image width
	 */
	public static final int IMAGE_WIDTH_1 = 0;

	/**
	 * Image height
	 */
	public static final int IMAGE_HEIGHT_1 = 0;

	/**
	 * A test start message
	 */
	public static final String START_MESSAGE = "{type:start}";

	/**
	 * A test stop message
	 */
	public static final String STOP_MESSAGE = "{type:stop}";

	/**
	 * Field name of the feed runner
	 */
	public static final String FEED_RUNNER_FIELD = "feedRunner";

	/**
	 * A test invalid message
	 */
	public static final String INVALID_MESSAGE = "{invalid:true}";

	/**
	 * A simple test message to check setup
	 */
	public static final String TEST_MESSAGE = "Hello world!";
}
