package uk.ac.surrey.cs.tvs.vvote.client;

import org.junit.*;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowService;

import uk.ac.surrey.cs.tvs.CustomShadowTestRunner;
import uk.ac.surrey.cs.tvs.vvote.VVoteService;

import fi.iki.elonen.NanoHTTPD.Response;

import static org.junit.Assert.*;

/**
 * The class <code>BarcodeServletTest</code> contains tests for the class
 * <code>{@link BarcodeServlet}</code>.
 */
@RunWith(CustomShadowTestRunner.class)
public class BarcodeServletTest {

	/**
	 * Holds a reference to the service used throughout
	 */
	private static VVoteServiceInterface service = null;

	/**
	 * Perform pre-test initialization.
	 * 
	 * @throws Exception
	 *             if the initialization fails for some reason
	 */
	@Before
	public void setUp() throws Exception {
		service = Robolectric.buildService(VVoteService.class).create().get();
	}

	/**
	 * Perform post-test clean-up.
	 * 
	 * @throws Exception
	 *             if the clean-up fails for some reason
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}

	/**
	 * Run the BarcodeServlet(VVoteServiceInterface) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBarcodeServlet_1() throws Exception {
		VVoteServiceInterface service = null;

		BarcodeServlet result = new BarcodeServlet(service);

		assertNotNull(result);
	}

	/**
	 * Run the BarcodeServlet(VVoteServiceInterface) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBarcodeServlet_2() throws Exception {
		BarcodeServlet result = new BarcodeServlet(service);

		assertNotNull(result);
	}

	/**
	 * Run the boolean cameraDetected() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCameraDetected_1() throws Exception {
		BarcodeServlet fixture = new BarcodeServlet(service);

		boolean result = fixture.cameraDetected();
		assertEquals(false, result);
	}

	/**
	 * Run the fi.iki.elonen.NanoHTTPD.Response
	 * runServlet(String,Method,Map<String
	 * ,String>,Map<String,String>,Map<String,String>,File) method test.
	 * 
	 * @throws Exception
	 */
	// TODO: Unable to fully test this method - failing at line 98 .runServlet
	@Test
	@Config(shadows = { ShadowWebcamManager.class, ShadowService.class })
	public void testRunServlet_1() throws Exception {
		BarcodeServlet fixture = new BarcodeServlet(service);

		Response result = fixture
				.runServlet(null, null, null, null, null, null);

		assertNotNull(result);
	}
}