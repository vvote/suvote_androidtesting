package uk.ac.surrey.cs.tvs.vvote;

import org.robolectric.Robolectric;

import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.vvote.client.EpsonPrinter;
import uk.ac.surrey.cs.tvs.vvote.client.VVoteServiceInterface;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

/**
 * Stubbed version of the VVoteService
 */
public class VVoteService extends Service implements VVoteServiceInterface {

	/**
	 * Epson printer
	 */
	private EpsonPrinter ep;

	/**
	 * Config file
	 */
	private ConfigFile config;

	/**
	 * used to load the config file if not null;
	 */
	private String configFile = null;

	/**
	 * Sets the name of the config file to use
	 * 
	 * @param configFile
	 */
	public void setConfigName(String configFile) {
		this.configFile = configFile;
	}

	@Override
	public ConfigFile getConfig() {
		try {
			if (this.config == null) {
				if (this.configFile == null) {
					this.config = new ConfigFile();
				} else {
					this.config = new ConfigFile(this.configFile);
				}
			}
			return this.config;
		} catch (JSONIOException e) {
			return null;
		}
	}

	@Override
	public EpsonPrinter getEpsonPrinter() {
		if (this.ep == null) {
			EpsonPrinter.ConnectionType type = EpsonPrinter.ConnectionType.USB;
			Context ctx = Robolectric.application;

			this.ep = new EpsonPrinter(type, ctx);
		}
		return this.ep;
	}

	@Override
	public Service getService() {
		return this;
	}

	@Override
	public EpsonPrinter reconnectPrinter() {
		return this.ep;
	}

	@Override
	public void sendStatus() {
		// stub
	}

	@Override
	public void setEpsonPrinter(EpsonPrinter updatedPrinter) {
		// stub
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
