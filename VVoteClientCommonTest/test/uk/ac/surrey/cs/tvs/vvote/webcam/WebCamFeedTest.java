package uk.ac.surrey.cs.tvs.vvote.webcam;

import java.lang.reflect.Field;

import uk.ac.surrey.cs.tvs.CustomShadowTestRunner;
import uk.ac.surrey.cs.tvs.vvote.ClientCommonVVoteTestParameters;
import uk.ac.surrey.cs.tvs.vvote.VVoteService;
import uk.ac.surrey.cs.tvs.vvote.client.VVoteServiceInterface;
import org.junit.*;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.vvote.json.JSONException;
import org.vvote.json.JSONObject;

/**
 * The class <code>WebCamFeedTest</code> contains tests for the class
 * <code>{@link WebCamFeed}</code>.
 */
@RunWith(CustomShadowTestRunner.class)
public class WebCamFeedTest {

	/**
	 * Service stub
	 */
	private static VVoteServiceInterface vVoteServiceStub;

	/**
	 * Web socket stub
	 */
	private static WebSocketStub webSocketStub;

	/**
	 * A start message
	 */
	private static JSONObject startMessage = null;

	/**
	 * A stop message
	 */
	private static JSONObject stopMessage = null;

	/**
	 * An invalid message
	 */
	private static JSONObject invalidMessage = null;

	/**
	 * Setup for the test cases
	 * 
	 * @throws JSONException
	 */
	@BeforeClass
	public static void oneTimeSetUp() throws JSONException {
		vVoteServiceStub = new VVoteService();

		webSocketStub = new WebSocketStub();

		startMessage = new JSONObject(
				ClientCommonVVoteTestParameters.START_MESSAGE);
		stopMessage = new JSONObject(
				ClientCommonVVoteTestParameters.STOP_MESSAGE);
		invalidMessage = new JSONObject(
				ClientCommonVVoteTestParameters.INVALID_MESSAGE);
	}

	/**
	 * Tear down for test cases
	 */
	@AfterClass
	public static void oneTimeTearDown() {
		vVoteServiceStub = null;
		webSocketStub = null;
	}

	/**
	 * Run the WebCamFeed(VVoteServiceInterface) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testWebCamFeed_1() throws Exception {
		VVoteServiceInterface service = null;

		WebCamFeed result = new WebCamFeed(service);

		assertNotNull(result);
	}

	/**
	 * Run the WebCamFeed(VVoteServiceInterface) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testWebCamFeed_2() throws Exception {
		WebCamFeed result = new WebCamFeed(vVoteServiceStub);

		assertNotNull(result);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testProcessMessage_1() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(null, null);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testProcessMessage_2() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(new JSONObject(), null);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = NullPointerException.class)
	public void testProcessMessage_3() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(null, webSocketStub);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testProcessMessage_4() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(new JSONObject(), webSocketStub);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowFeedRunner.class })
	public void testProcessMessage_5() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		Field field = fixture.getClass().getDeclaredField(
				ClientCommonVVoteTestParameters.FEED_RUNNER_FIELD);
		field.setAccessible(true);
		FeedRunner feedRunner = (FeedRunner) field.get(fixture);

		assertNull(feedRunner);

		fixture.processMessage(startMessage, webSocketStub);

		feedRunner = (FeedRunner) field.get(fixture);

		ShadowFeedRunner shadowFeedRunner = Robolectric.shadowOf_(feedRunner);

		assertTrue(shadowFeedRunner.hasStartBeenCalled());
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testProcessMessage_6() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(stopMessage, webSocketStub);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testProcessMessage_7() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(invalidMessage, webSocketStub);
	}

	/**
	 * Run the void processMessage(JSONObject,WebSocket) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testProcessMessage_8() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(stopMessage, webSocketStub);
		fixture.processMessage(stopMessage, webSocketStub);
	}

	/**
	 * Run the void stop() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowFeedRunner.class })
	public void testStop_1() throws Exception {
		WebCamFeed fixture = new WebCamFeed((VVoteServiceInterface) null);
		assertNotNull(fixture);

		fixture.stop();
	}

	/**
	 * Run the void stop() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	@Config(shadows = { ShadowFeedRunner.class })
	public void testStop_2() throws Exception {
		WebCamFeed fixture = new WebCamFeed(vVoteServiceStub);
		assertNotNull(fixture);

		fixture.processMessage(startMessage, webSocketStub);

		Field field = fixture.getClass().getDeclaredField(
				ClientCommonVVoteTestParameters.FEED_RUNNER_FIELD);
		field.setAccessible(true);
		FeedRunner feedRunner = (FeedRunner) field.get(fixture);

		ShadowFeedRunner shadowFeedRunner = Robolectric.shadowOf_(feedRunner);

		assertFalse(shadowFeedRunner.hasStopBeenCalled());

		fixture.stop();

		assertTrue(shadowFeedRunner.hasStopBeenCalled());
	}
}