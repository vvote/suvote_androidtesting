package uk.ac.surrey.cs.tvs.vvote.client;

import android.os.Environment;
import junit.framework.TestCase;

/**
 * The class <code>AndroidConstantsTest</code> contains tests for the class
 * <code>{@link AndroidConstants}</code>.
 */
public class AndroidConstantsTest extends TestCase {

	/**
	 * Holds location of application folder
	 */
	private static String APP_FOLDER;

	protected void setUp() {
		APP_FOLDER = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/vvoteclient/";
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsConfig_1() throws Exception {
		assertEquals("serverRoot", AndroidConstants.Config.SERVER_ROOT);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsConfig_2() throws Exception {
		assertEquals("serverPort", AndroidConstants.Config.SERVER_PORT);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsConfig_3() throws Exception {
		assertEquals("printDebug", AndroidConstants.Config.PRINT_DEBUG);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsConfig_4() throws Exception {
		assertEquals("debugDir", AndroidConstants.Config.DEBUG_DIR);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsConfig_5() throws Exception {
		assertEquals("printerBuffer", AndroidConstants.Config.PRINT_BUFFER);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsConfig_6() throws Exception {
		assertEquals("barcodeTimeout", AndroidConstants.Config.BARCODE_TIMEOUT);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsPaths_1() throws Exception {
		assertEquals(APP_FOLDER, AndroidConstants.Paths.APP_FOLDER);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsPaths_2() throws Exception {
		assertEquals(APP_FOLDER + "config.json",
				AndroidConstants.Paths.CONFIG_FILE);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsPaths_3() throws Exception {
		assertEquals(APP_FOLDER + "client.conf",
				AndroidConstants.Paths.CLIENT_CONFIG);
	}

	/**
	 * Test the android constants lookup.
	 * 
	 * @throws Exception
	 */
	public void testAndroidConstantsPaths_4() throws Exception {
		assertEquals(APP_FOLDER + "districtconf.json",
				AndroidConstants.Paths.DISTRICT_CONFIG);
	}
}
