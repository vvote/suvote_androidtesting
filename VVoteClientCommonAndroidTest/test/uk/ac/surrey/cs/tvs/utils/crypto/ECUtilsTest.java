package uk.ac.surrey.cs.tvs.utils.crypto;

import java.math.BigInteger;
import java.security.SecureRandom;

import junit.framework.TestCase;

import org.vvote.bouncycastle.math.ec.ECPoint;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * The class <code>ECUtilsEncryptionTest</code> contains tests for the class
 * <code>{@link ECUtils}</code>.
 */
public class ECUtilsTest extends TestCase{

	/**
	 * Run the ECPoint[] encrypt(ECPoint,ECPoint) method test.
	 * 
	 * @throws Exception
	 */
	public void testEncrypt_1() throws Exception {
		// get fixture
		ECUtils fixture = new ECUtils();

		// get a plaintext
		ECPoint plainText = fixture.getRandomValue();
		
		// get a private key
		BigInteger privateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get corresponding public key
		ECPoint publicKey = fixture.getG().multiply(privateKey);

		// get encryption
		ECPoint[] result = fixture.encrypt(plainText, publicKey);

		// calculate decryption
		ECPoint decrypt = result[1]
				.add(result[0].multiply(privateKey).negate());

		assertNotNull(result);
		assertEquals(plainText, decrypt);
	}
	
	/**
	 * Run the ECPoint[] encrypt(ECPoint,ECPoint,BigInteger) method test.
	 * 
	 * @throws Exception
	 */
	public void testEncrypt_2() throws Exception {
		// get fixture
		ECUtils fixture = new ECUtils();

		// get a plaintext
		ECPoint plainText = fixture.getRandomValue();
		
		// get a private key
		BigInteger privateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get corresponding public key
		ECPoint publicKey = fixture.getG().multiply(privateKey);

		// compute a randomness value for use in encryption
		BigInteger randomness = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());

		// get encryption
		ECPoint[] result = fixture.encrypt(plainText, publicKey, randomness);

		// calculate decryption
		ECPoint decrypt = result[1]
				.add(result[0].multiply(privateKey).negate());

		assertNotNull(result);
		assertEquals(plainText, decrypt);
	}
	
	/**
	 * Run the ECPoint[] encrypt(ECPoint,ECPoint) method test.
	 * 
	 * @throws Exception
	 */
	public void testEncrypt_3() throws Exception {
		// get fixture
		ECUtils fixture = new ECUtils();

		// get a plaintext
		ECPoint plainText = fixture.getRandomValue();
		
		// get a private key
		BigInteger privateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get another private key
		BigInteger otherPrivateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get corresponding public key
		ECPoint publicKey = fixture.getG().multiply(privateKey);

		// get encryption
		ECPoint[] result = fixture.encrypt(plainText, publicKey);

		// calculate decryption using the other private key
		ECPoint decrypt = result[1]
				.add(result[0].multiply(otherPrivateKey).negate());

		assertNotNull(result);
		assertNotSame(plainText, decrypt);
	}
	
	/**
	 * Run the ECPoint[] encrypt(ECPoint,ECPoint) method test.
	 * 
	 * @throws Exception
	 */
	public void testEncrypt_4() throws Exception {
		// get fixture
		ECUtils fixture = new ECUtils();

		// get a plaintext
		ECPoint plainText = fixture.getRandomValue();
		
		// get a private key
		BigInteger privateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get another private key
		BigInteger otherPrivateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get corresponding public key
		ECPoint publicKey = fixture.getG().multiply(otherPrivateKey);

		// get encryption - check that the encryption relies on the public key
		ECPoint[] result = fixture.encrypt(plainText, publicKey);

		// calculate decryption using original private key
		ECPoint decrypt = result[1]
				.add(result[0].multiply(privateKey).negate());

		assertNotNull(result);
		assertNotSame(plainText, decrypt);
	}
	
	/**
	 * Run the ECPoint[] reencrypt(ECPoint[],ECPoint,BigInteger) method test.
	 * 
	 * @throws Exception
	 */
	public void testReencrypt_1() throws Exception {
		ECUtils fixture = new ECUtils();
		
		// get plaintext
		ECPoint plainText = fixture.getRandomValue();
		
		// get private key
		BigInteger privateKey = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// get matching public key
		ECPoint publicKey = fixture.getG().multiply(privateKey);

		// first encryption
		ECPoint[] encryptedCipher = fixture.encrypt(plainText, publicKey);
		
		BigInteger randomness = fixture.getRandomInteger(
				fixture.getOrderUpperBound(), new SecureRandom());
		
		// reencrypt cipher using provided randomness
		ECPoint[] reencryptedCipherTest = fixture.reencrypt(encryptedCipher, publicKey, randomness);
		
		ECPoint[] reencryptedCipher = new ECPoint[2];
		reencryptedCipher[0] = encryptedCipher[0].add(fixture.getG().multiply(randomness));
		reencryptedCipher[1] = encryptedCipher[1].add(publicKey.multiply(randomness));
		
		assertEquals(reencryptedCipher[0], reencryptedCipherTest[0]);
		assertEquals(reencryptedCipher[1], reencryptedCipherTest[1]);
		
		// calculate decryption using original private key
		ECPoint decrypt = reencryptedCipherTest[1]
				.add(reencryptedCipherTest[0].multiply(privateKey).negate());

		assertNotSame(plainText, decrypt);
	}
}
