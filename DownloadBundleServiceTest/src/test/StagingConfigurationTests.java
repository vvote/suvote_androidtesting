package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;

import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.slf4j.LoggerFactory;

import android.os.Environment;
import au.gov.vic.vec.StagingConfiguration;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.joran.spi.JoranException;

@RunWith(RobolectricTestRunner.class)
public class StagingConfigurationTests {

	protected static String jsonPath = "/vVoteClient/vvwww/stagingconfig.json";
	protected static String jsonString = "{\"lastName\":\"Smith\",\"firstName\":\"John\"}";
	protected static String badJsonString = "{,{\"lastName\":\"Smith\",\"firstName\":\"John\"}";
	
	private String logPath = "logs/testLog.log";
	private String ioMsg = "IOException whilst reading config file";
	private String jsonMsg = "JSONException whilst reading config file";
	
	// ISO8601 Formatted Dates
	String date = "2014-07-23";
    String dateAndTime1 = "2014-07-23T08:39:14+00:00";
    String dateAndTime2 = "2014-07-23T08:39:14Z";
    String dateAndTimeOffset = "2014-07-23T12:39:14+04";
    String week = "2014-W30";
    String weekAndDay = "2014-W30-3";
    String ordinal = "2014-204";
	
	@Before
	 public void setupLogging() {

		File logFile = new File(logPath);
 		if (logFile.exists()) logFile.delete();
		
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		lc.reset();
		
        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        ple.setPattern("%msg%n");
        ple.setContext(lc);
        ple.start();
        
        FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
        fileAppender.setFile(logPath);
        fileAppender.setEncoder(ple);
        fileAppender.setContext(lc);
        fileAppender.start();
        
        Logger logger = (Logger) LoggerFactory.getLogger(StagingConfiguration.class);
        logger.addAppender(fileAppender);
        logger.setLevel(Level.DEBUG);
        logger.setAdditive(false);
     }
	
	@Test
	public void toCalendarShouldParseISO8601FormattedDates() throws ParseException {
		
		StagingConfiguration.toCalendar(dateAndTime1);
		StagingConfiguration.toCalendar(dateAndTime2);
		
		// unsupported formats
		/*
		StagingConfiguration.toCalendar(date);
		StagingConfiguration.toCalendar(dateAndTimeOffset);
		StagingConfiguration.toCalendar(week);
		StagingConfiguration.toCalendar(weekAndDay);
		StagingConfiguration.toCalendar(ordinal);
		*/
	}
	
 	@Test
    public void toCalendarShouldSetCorrectDate() {

        // acceptable margin of error between calendars in ms
        long margin = 60*1000;
        
        Calendar cal1 = Calendar.getInstance();
        cal1.set(2014, 6, 23, 8, 39, 14);
        Assert.assertTrue(calendarsMatch(cal1, dateAndTime1, margin));
        Assert.assertTrue(calendarsMatch(cal1, dateAndTime2, margin));
        
        // unsupported formats
        /*
        cal1.set(2014, 6, 21);
        Assert.assertTrue(calendarsMatch(cal1, week, margin));
        cal1.set(2014, 6, 23);
        Assert.assertTrue(calendarsMatch(cal1, date, margin));
        Assert.assertTrue(calendarsMatch(cal1, weekAndDay, margin));
        Assert.assertTrue(calendarsMatch(cal1, ordinal, margin));
        cal1.set(2014, 6, 23, 8, 39, 14);
        Assert.assertTrue(calendarsMatch(cal1, dateAndTimeOffset, margin));
        */
    }

	@Test
 	public void constructorShouldLogIOExceptionIfNoConfig() {
 		new StagingConfiguration();
 		Assert.assertTrue(logContains(ioMsg));
 	}

	@Test
 	public void constructorShouldLogJSONExceptionIfBadConfig() {
 		setJsonConfigFile(badJsonString);
 		new StagingConfiguration();
 		Assert.assertTrue(logContains(jsonMsg));
 	}
 	
 	@Test
 	public void constructorShouldLogNoErrorsIfGoodConfig() {
 		setJsonConfigFile(jsonString);
 		new StagingConfiguration();
 		Boolean error = logContains(ioMsg) || logContains(jsonMsg);
 		Assert.assertFalse(error);
 	}
 	
 	@Test
 	public void getStagingConfigurationDataShouldReturnJSON() {
 		setJsonConfigFile(jsonString);
 		StagingConfiguration sc = new StagingConfiguration();
 		JSONObject json = sc.getStagingConfigurationData();
 		Assert.assertTrue(json.toString().equals(jsonString));
 	}
 	
 	@After
 	public void clearConfigFile() {
 		deleteJsonConfigFile();	
 	}
 	
 	@AfterClass
 	public static void resetLogging() {
 		
 		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory(); 
 	    ContextInitializer ci = new ContextInitializer(lc); 
 	    lc.reset();
 	    try {   
 	        ci.autoConfig();  
 	    } catch (JoranException e) {  
 	        e.printStackTrace(); 
 	    }
 	}
 	
 	private boolean calendarsMatch(Calendar cal1, String isoString, long margin) {
 		
 		Calendar cal2;
 		long diff = 0;
		try {
			cal2 = StagingConfiguration.toCalendar(isoString);
			diff = cal1.getTimeInMillis() - cal2.getTimeInMillis();
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return diff < margin;
	}
 	
 	private Boolean logContains(String message) {
 
 		Boolean found = false;
		try {
			FileReader logFileReader = new FileReader(logPath);
			BufferedReader bufReader = new BufferedReader(logFileReader);
			String line;
			while ((line = bufReader.readLine()) != null) {
				   if (line.contains(message)) found = true;
				}
			bufReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
 		
		return found;
	}
 	
 	protected static void setJsonConfigFile(String json) {
 		
 		String jsonPathFull = Environment.getExternalStorageDirectory().getPath() + jsonPath;
 		File file = new File(jsonPathFull);
		file.getParentFile().mkdirs();
		FileWriter writer;
		try {
			writer = new FileWriter(file);
			writer.write(json);
			writer.close();
			} catch (IOException e) {
			e.printStackTrace();
		}
 	}
 	
 	protected static void deleteJsonConfigFile() {
 		
 		String jsonPathFull = Environment.getExternalStorageDirectory().getPath() + jsonPath;
 		File configFile = new File(jsonPathFull);
 		if (configFile.exists()) configFile.delete();
 	}
}
