package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import au.gov.vic.vec.DownloadBundleService;
import au.gov.vic.vec.DownloadResult;

@RunWith(RobolectricTestRunner.class)
public class DownloadBundleServiceTests {
    
	private String goodConfig = "{'EventEntity':{'JSON': 'Hello, World'},'DeviceId':'1234'}";
	
	@Test
	public void onHandleIntent_REQUEST_LAST_DOWNLOAD_shouldBroadcastCorrectIntent() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.REQUEST_LAST_DOWNLOAD,
				new BasicNameValuePair("nextScheduled", "Downloaded"), // line included to avoid call to scheduleNextRun()
				new BasicNameValuePair("lastDownload", "testDownload"));
        
        assertEquals(1, Robolectric.getShadowApplication().getBroadcastIntents().size());
        
        assertIntentsWereBroadcast(
        		new BasicNameValuePair(DownloadBundleService.REQUEST_LAST_DOWNLOAD, "testDownload"));
	}

	@Test
	public void onHandleIntent_REQUEST_STATUS_shouldBroadcastCorrectIntent() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.REQUEST_STATUS,
				new BasicNameValuePair("nextScheduled", "Downloaded"),
				new BasicNameValuePair("lastStatus", "testStatus"));
        
        assertEquals(1, Robolectric.getShadowApplication().getBroadcastIntents().size());
        
        assertIntentsWereBroadcast(
        		new BasicNameValuePair(DownloadBundleService.REQUEST_STATUS, "testStatus"));
	}
	
	@Test
	public void onHandleIntent_REQUEST_NEXT_SCHEDULED_shouldBroadcastCorrectIntent() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.REQUEST_NEXT_SCHEDULED,
				new BasicNameValuePair("nextScheduled", "Downloaded"));
        
        assertEquals(1, Robolectric.getShadowApplication().getBroadcastIntents().size());
        
        assertIntentsWereBroadcast(
        		new BasicNameValuePair(DownloadBundleService.REQUEST_NEXT_SCHEDULED, "Downloaded"));
	}
	
	@Test
	public void onHandleIntent_REQUEST_ALL_INFO_shouldBroadcastCorrectIntents() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.REQUEST_ALL_INFO,
				new BasicNameValuePair("nextScheduled", "Downloaded"),
				new BasicNameValuePair("lastDownload", "testDownload"),
				new BasicNameValuePair("lastStatus", "testStatus"));
        
        assertEquals(3, Robolectric.getShadowApplication().getBroadcastIntents().size());
        
        assertIntentsWereBroadcast(
        		new BasicNameValuePair(DownloadBundleService.REQUEST_NEXT_SCHEDULED, "Downloaded"),
        		new BasicNameValuePair(DownloadBundleService.REQUEST_LAST_DOWNLOAD, "testDownload"),
        		new BasicNameValuePair(DownloadBundleService.REQUEST_STATUS, "testStatus"));
	}
	
	@Test
	public void onHandleIntent_SCHEDULED_ShouldBroadcastCorrectIntentIfLastStatusSuccess() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.SCHEDULED,
				new BasicNameValuePair("nextScheduled", "Downloaded"),
				new BasicNameValuePair("lastStatus", "SUCCESS"));
        
        assertEquals(1, Robolectric.getShadowApplication().getBroadcastIntents().size()); 
        
        assertIntentsWereBroadcast(
        		new BasicNameValuePair(DownloadBundleService.REQUEST_NEXT_SCHEDULED, "Downloaded"));
	}
	
	@Test
	public void onHandleIntent_SCHEDULED_ShouldBroadcastCorrectIntentIfNoLastStatus() {
		/*
		 *  Can't test - NullPointerException - DownloadBundleService line 564
		 *  AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		 */
	}
	
	@Test
	public void onHandleIntent_REQUEST_DOWNLOAD_shouldBroadcastNoIntentsIfLastStatusSuccess() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.REQUEST_DOWNLOAD,
				new BasicNameValuePair("nextScheduled", "Downloaded"),
				new BasicNameValuePair("lastStatus", "SUCCESS"));
        
        assertEquals(0, Robolectric.getShadowApplication().getBroadcastIntents().size());
	}
	
	@Test
	public void onHandleIntent_REQUEST_DOWNLOAD_shouldBroadcastCorrectIntentsIfNoLastStatus() {
		
		callOnHandleIntentWithRequest(DownloadBundleService.REQUEST_DOWNLOAD,
				new BasicNameValuePair("nextScheduled", "Downloaded"));

        assertEquals(4, Robolectric.getShadowApplication().getBroadcastIntents().size());
        
        assertIntentsWereBroadcast(
        		new BasicNameValuePair("download", DownloadResult.EXCEPTION.name()),
        		new BasicNameValuePair(DownloadBundleService.REQUEST_LAST_DOWNLOAD, "*"),
        		new BasicNameValuePair(DownloadBundleService.REQUEST_NEXT_SCHEDULED, "Downloaded"),
        		new BasicNameValuePair(DownloadBundleService.REQUEST_STATUS, DownloadResult.EXCEPTION.name()));
	}
	
	@Test
	public void pollDownloadShouldReturnExceptionIfNoStagingConfiguration() {
		
		DownloadBundleService service = new DownloadBundleService();
		DownloadResult result = service.pollDownload();
		assertEquals(DownloadResult.EXCEPTION, result);
	}
	
	@Test
	public void pollDownloadShouldReturnExceptionIfInvalidStagingConfiguration() {
		
		StagingConfigurationTests.setJsonConfigFile(StagingConfigurationTests.jsonString);
		DownloadBundleService service = new DownloadBundleService();
		DownloadResult result = service.pollDownload();
		assertEquals(DownloadResult.EXCEPTION, result);
		StagingConfigurationTests.deleteJsonConfigFile();
	}
	
	@Test
	public void pollDownloadShouldReturnFailIfLastStatusSuccess() {
		
		StagingConfigurationTests.setJsonConfigFile(goodConfig);
		DownloadBundleService service = new DownloadBundleService();
		setDownloadDetails(service,
				new BasicNameValuePair("nextScheduled", "Downloaded"),
				new BasicNameValuePair("lastStatus", "SUCCESS"));
		DownloadResult result = service.pollDownload();
		assertEquals(DownloadResult.FAIL, result);
		StagingConfigurationTests.deleteJsonConfigFile();
	}

	private void callOnHandleIntentWithRequest(String request, BasicNameValuePair... downloadDetails) {
		
		Intent serviceIntent = createIntentWithBundle(
				DownloadBundleService.REQUEST_STRING, request);
		DownloadBundleServiceMock service = new DownloadBundleServiceMock();
		setDownloadDetails(service, downloadDetails);
        service.onHandleIntent(serviceIntent);
	}
	
	private void setDownloadDetails(DownloadBundleService service, BasicNameValuePair... pairs) {
		
		SharedPreferences prefs = service.getSharedPreferences
				("DOWNLOAD_DETAILS", Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        for (BasicNameValuePair pair : pairs) {
        	editor.putString(pair.getName(), pair.getValue());
        }
        editor.commit();
        service.onCreate();
	}
	
	private Intent createIntentWithBundle(String key, String value) {
		
		Bundle bundle = new Bundle();
        bundle.putString(key, value);
        Intent serviceIntent = new Intent(Robolectric.application, DownloadBundleServiceMock.class);
        serviceIntent.putExtras(bundle);
        Robolectric.getShadowApplication().startService(serviceIntent);
        return serviceIntent;
	}
	
	private void assertIntentsWereBroadcast(BasicNameValuePair... pairs) {
		
		List<Intent> intentList = Robolectric.getShadowApplication().getBroadcastIntents();
		
		for (BasicNameValuePair pair : pairs) {
			if (!intentIsInList(intentList, pair))
				fail("Intent not broadcast: " + pair.getName());
		}
	}
	
    private Boolean intentIsInList(List<Intent> intentList, BasicNameValuePair pair) {
		
    	for (Intent i : intentList) {
    		String extra1 = i.getStringExtra(DownloadBundleService.RESPONSE_STRING);
            String extra2 = i.getStringExtra(DownloadBundleService.RESPONSE_MESSAGE);
            if (extra1.equals(pair.getName()) &&
            	(pair.getValue().equals("*") ? true : extra2.equals(pair.getValue())))
            	return true;
    	}
    	return false;
	}

	private class DownloadBundleServiceMock extends DownloadBundleService {
        @Override
        public void onHandleIntent(Intent intent) {
            super.onHandleIntent(intent);
        }
    }
}

